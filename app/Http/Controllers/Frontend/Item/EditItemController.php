<?php

namespace App\Http\Controllers\Frontend\Item;

use App\Http\Controllers\Controller;
use App\Http\Resources\ItemResource;
use App\Models\Item;
use Illuminate\Http\Request;
use Inertia\Inertia;

class EditItemController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, Item $item)
    {
        /**
         * Untuk mengambil id user yang login
         * $request->user()->id
         * Auth::id()
         * auth()->id()
         */
        
        // if ($request->user()->id !== $item->user_id) {
        //     abort(403);
        // }

        $this->authorize('update', $item);

        return Inertia::render('Frontend/Item/EditItem', [
            'item' => ItemResource::make($item),
        ]);
    }
}
