<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ListUserController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // return User::latest('id')->paginate(5);

        // if ($request->search) {
        //     $users = $users->where('name', 'like', '%' . $request->search . '%');
        // }

        return Inertia::render('Admin/User/ListUser', [
            'users' => User::query()
                ->when($request->search, function ($query) use ($request) {
                    $query->where('name', 'like', '%' . $request->search . '%');
                })
                ->when($request->role, function ($query) use ($request) {
                    $query->where('role', $request->role);
                })
                ->latest('id')
                ->paginate(5)
                ->withQueryString(),
            'filters' => $request->only('search', 'role')
        ]);
    }
}
