<?php

namespace Tests\Feature;

use App\Models\Item;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ShowItemTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_show_item_increasing_views_count()
    {
        $user = User::factory()->create();
        $item = Item::factory()->create(['user_id' => $user->id]);
        
        $response = $this->get(route('item.show', $item));

        $this->assertDatabaseHas('items', [
            'id' => $item->id,
            'views' => $item->views + 1,
        ]);

        $response->assertStatus(200);
    }
}
