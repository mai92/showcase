<?php

namespace Tests\Feature;

use App\Models\Item;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ItemDeleteTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_an_user_can_delete_their_item()
    {
        $user = User::factory()->create();
        $item = Item::factory()->create(['user_id' => $user->id]);

        $this->assertDatabaseHas('items', [
            'id' => $item->id,
            'user_id' => $user->id,
        ]);

        $this->actingAs($user);

        $response = $this->delete("/item/{$item->id}");
        // hapus data sesuai id item

        $this->assertDatabaseMissing('items', [
            'id' => $item->id,
            'user_id' => $user->id,
        ]);
    }

    public function test_an_user_cannot_delete_others_item()
    {
        $user = User::factory()->create();
        $user2 = User::factory()->create();
        $item = Item::factory()->create(['user_id' => $user->id]);

        $this->assertDatabaseHas('items', [
            'id' => $item->id,
            'user_id' => $user->id,
        ]);

        $this->actingAs($user2); // fungsi seolah user2 login

        $response = $this->delete("/item/{$item->id}");
        
        $response->assertStatus(403);
    }

    public function test_an_admin_delete_users_item()
    {
        $user = User::factory()->create();
        $admin = User::factory()->create(['role' => 'admin']);
        $item = Item::factory()->create(['user_id' => $user->id]);

        $this->assertDatabaseHas('items', [
            'id' => $item->id,
            'user_id' => $user->id,
        ]);

        $this->actingAs($admin);

        $response = $this->delete("/item/{$item->id}");

        $this->assertDatabaseMissing('items', [
            'id' => $item->id,
            'user_id' => $user->id,
        ]);
        
        $response->assertStatus(200);
    }
}
