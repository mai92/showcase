<?php

namespace Tests\Feature;

use App\Models\Item;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExampleTest extends TestCase
{   
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_the_application_returns_a_successful_response()
    {
        User::factory()->create();
        Item::factory()->count(10)->create();
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function test_homepage_cannot_be_loaded_without()
    {
        $response = $this->get('/');

        $response->assertStatus(500);
    }
}
