<?php

namespace Tests\Feature;

use App\Models\Item;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ItemDetailTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_can_see_item_detail_page()
    {
        $itemId = 1;
        $user = User::factory()->create();
        $item = Item::factory()->create([
            'id' => $itemId,
            'user_id' => $user->id,
        ]);

        $response = $this->get(route('item.show', $item->id));

        $response->assertStatus(200);
    }
}
