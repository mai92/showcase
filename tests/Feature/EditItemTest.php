<?php

namespace Tests\Feature;

use App\Models\Item;
use App\Models\User;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use LogicException;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use SebastianBergmann\RecursionContext\InvalidArgumentException;
use PHPUnit\Framework\ExpectationFailedException;
use Tests\TestCase;

class EditItemTest extends TestCase
{
    use RefreshDatabase;

    private $user1;
    private $item;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user1 = User::factory()->create();
        $this->item = Item::factory()->create(['user_id' => $this->user1->id]);
    }
    
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_an_user_can_edit_their_item()
    {   
        $this->actingAs($this->user1);
        $response = $this->get(route('item.edit', $this->item->id));

        $response->assertStatus(200);
    }
    
    /**
     * @test
     */
    public function userCannotEditOthersItem()
    {
        $user2 = User::factory()->create();

        $this->actingAs($user2);
        $response = $this->get(route('item.edit', $this->item->id));

        $response->assertStatus(403);
    }
}
