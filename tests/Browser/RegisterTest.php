<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class RegisterTest extends DuskTestCase
{
    use DatabaseMigrations;

    /**
     * A Dusk test example.
     * 
     * @group register
     * @return void
     */
    public function test_successfully_visit_register_page()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/register')
                    ->assertSee('REGISTER');
        });
    }

    /**
     * A Dusk test example.
     * 
     * @group register
     * @return void
     */
    public function test_user_can_register_successfully()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/register')
                    ->type('name', 'amirul')
                    ->type('username', 'amirul')
                    ->type('email', 'amirul@bwa.test')
                    ->type('password', 'password')
                    ->type('password_confirmation', 'password')
                    ->click('button[type="submit"]')
                    ->waitForLocation('/dashboard')
                    ->assertPathIs('/dashboard');
        });
    }
}
